<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_location');
            $table->string('property_name');
            $table->string('property_price');
            $table->string('bathrooms');
            $table->string('bedrooms');
            $table->string('plotsize');
            $table->string('property_status');
            $table->string('property_mortgagerange');
            $table->longText('property_indetails');
            $table->string('property_image');
            $table->string('property_developer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
