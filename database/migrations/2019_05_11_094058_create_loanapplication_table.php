<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanapplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanapplication', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->string('property_id');
            $table->longText('credit_score_comment')->nullable();
            $table->string('credit_score_status')->nullable();
            $table->string('financial_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanapplication');
    }
}
