<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = "developer";
    protected $fillable = ['id','developer_name','developer_phonenumber','developer_address','developer_email',''];
}
