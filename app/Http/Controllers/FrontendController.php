<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function contact(){
        return view('contact');
    }
    public function SendEmail(Request $request){
        $all = $request->all();

        $name = $request['name'];
        $email = "contact.metalrec@gmail.com";
        $from_email = $request['email'];
        $messageMail = $request['message'];
        $company_name = "Contact Metalrec";
        $subjectmail = $request['subject'];

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'subject' => $subjectmail,
                'messageMail' => $messageMail,
                'from' => $from_email
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$from_email)
            {
                $message->from($from_email);
                $message->to($email, $company_name)->subject($subjectmail);
            });

        return back()->with('success','Vous avez envoyé votre message avec succès, nous vous répondrons dans moins de 24 heures');

    }

}
