<!DOCTYPE html>

<html lang="en-US">

<head>
    <script src="cdn-cgi/apps/head/_jZ0jIuSj0S4kK9BleaWviaKHSc.js"></script>
    <script type="text/javascript">
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js'
    </script>

    <link type="text/css" media="all" href="wp-content/cache/autoptimize/1/css/autoptimize_a1c26e2b6904c7a320d895dbcc6cddf8.css" rel="stylesheet" />
    <link type="text/css" media="only screen and (max-width: 768px)" href="wp-content/cache/autoptimize/1/css/autoptimize_3be592c671f598b43e637562b04345ed.css" rel="stylesheet" />
    <title>@yield('title')</title>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

    <link rel="pingback" href="http://uplift.swiftideas.com/xmlrpc.php" />
    <script type="text/javascript">
        var yith_wcwl_plugin_ajax_web_url = '/wp-admin/admin-ajax.php';
    </script>
    <script>
        function sf_writeCookie() {
            the_cookie = document.cookie, the_cookie && window.devicePixelRatio >= 2 && (the_cookie = "sf_pixel_ratio=" + window.devicePixelRatio + ";" + the_cookie, document.cookie = the_cookie)
        }
        sf_writeCookie();
    </script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com' />
    <link rel='dns-prefetch' href='http://s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Feed" href="feed/index.rss" />
    <link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Comments Feed" href="comments/feed/index.rss" />
    <link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Home: Classic Comments Feed" href="feed/index.rss" />

    <meta property="og:title" content="Metalrec" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://uplift.swiftideas.com/home/home-classic/" />
    <meta property="og:site_name" content="Metalrec" />
    <meta property="og:description" content="">
    <meta property="og:image" content="wp-content/uploads/2014/12/uplift_logo.png" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Metalrec">
    <meta name="twitter:description" content="">
    <meta property="twitter:image:src" content="wp-content/uploads/2014/12/uplift_logo.png" />
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "http:\/\/uplift.swiftideas.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.6"
            }
        };
        ! function(a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case "flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case "emoji":
                        return b = d([55357, 56692, 8205, 9792, 65039], [55357, 56692, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }
            var g, h, i, j, k = b.createElement("canvas"),
                l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
                c.DOMReady = !0
            }, c.supports.everything || (h = function() {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function() {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <link rel='stylesheet' id='redux-google-fonts-sf_uplift_options-css' href='http://fonts.googleapis.com/css?family=Lato%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic%7COpen+Sans%3A300%2C400%2C600%2C700%2C800%2C300italic%2C400italic%2C600italic%2C700italic%2C800italic&amp;subset=latin&amp;ver=1474553898' type='text/css' media='all' />
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-ver=1.12.4.js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min-ver=1.4.1.js'></script>
    <script type='text/javascript' src='wp-content/themes/uplift/js/combine/plyr.js'></script>
    <link rel='https://api.w.org/' href='wp-json/index.json' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php-rsd.xml" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7Chome%7Chome-classic%7C.json" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7Chome%7Chome-classic%7C&amp;format=xml.xml" />
    <script type="text/javascript">
        var ajaxurl = 'http://uplift.swiftideas.com/wp-admin/admin-ajax.php';
    </script>
    <style>
        .ytp-title-enable-channel-logo .ytp-title {
            min-height: 52px;
            display: none !important;
        }
        .ytp-title-enable-channel-logo .ytp-title-channel {
            display: none !important;
        }
    </style>
    <!--[if lt IE 9]><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/respond.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/html5shiv.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/excanvas.compiled.js"></script><![endif]-->

</head>

<body class="page-template-default page page-id-13177 page-child parent-pageid-9  minimal-design mobile-header-center-logo mhs-tablet-land mh-sticky  mh-overlay responsive-fluid sticky-header-enabled page-shadow mobile-two-click standard product-shadows header-standard layout-fullwidth has-quickview-hover-btn disable-mobile-animations ">
<div id="site-loading" class="circle">
    <div class="sf-svg-loader">
        <object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
    </div>
</div>
<div id="container">
@yield('content')
</div>