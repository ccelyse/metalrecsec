<div id="mobile-menu-wrap" class="menu-is-left">
    <nav id="mobile-menu" class="clearfix">
        <div class="menu-main-menu-container">
            <ul id="menu-main-menu" class="menu">
                <li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">
                    <a href="{{'/'}}"><span class="menu-item-text">ACCUEIL</span></a>
                </li>
                <!--<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">-->
                <!--<a href=""><span class="menu-item-text">NOUS CONNAÎTRE</span></a>-->
                <!--</li>-->
                <!--<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">-->
                <!--<a href="CEQUEVOUSOFFREMETALREC.html"><span class="menu-item-text">CE QUE VOUS OFFRE METALREC</span></a>-->
                <!--</li>-->
                <li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">
                    <a href="{{'contact'}}"><span class="menu-item-text">CONTACT</span></a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<header id="mobile-header" class="mobile-center-logo clearfix">
    <div class="mobile-header-opts opts-left">
        <button class="hamburger mobile-menu-link hamburger--3dy" type="button">
                    <span class="hamburger-box">
<span class="hamburger-inner"></span>
                    </span>
        </button>
    </div>
    <div id="mobile-logo" class="logo-center has-img clearfix" data-anim="tada">
        <a href="index.html">
            <img class="standard" src="images/metalrec.png" alt="Uplift" height="30" width="77" />
            <img class="retina" src="images/metalrec.png" alt="Uplift" height="30" width="77" />
            <div class="text-logo"></div>
        </a>
    </div>
</header>

<div class="header-wrap  full-center full-header-stick page-header-standard" data-style="default" data-default-style="default">
    <div id="header-section" class="header-3 ">
        <header id="header" class="sticky-header clearfix">
            <div id="sf-full-header-search">
                <div class="container">
                    <form method="get" class="header-search-form" action="http://uplift.swiftideas.com/">
                        <input type="text" placeholder="Type and hit enter to search" name="s" autocomplete="off" />
                    </form>
                    <a href="index.html#" class="sf-fhs-close"><i class="sf-icon-remove-big"></i></a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div id="logo" class="logo-left has-img clearfix" data-anim="tada">
                        <a href="index.html">
                            <img class="standard" src="images/metalrec.png" alt="Uplift"/>
                            <img class="retina" src="images/metalrec.png" alt="Uplift"  />
                            <div class="text-logo"></div>
                        </a>
                    </div>
                    <div class="float-menu">
                        <nav id="main-navigation" class="std-menu clearfix">
                            <div id="mega-menu-wrap-main_navigation" class="mega-menu-wrap">
                                <div class="mega-menu-toggle">
                                    <div class='mega-toggle-block mega-menu-toggle-block mega-toggle-block-right' id='mega-toggle-block-1'></div>
                                </div>
                                <ul id="mega-menu-main_navigation" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
                                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>
                                        <a class="mega-menu-link" href="{{'/'}}">ACCUEIL</a>
                                    </li>
                                    <!--<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>-->
                                    <!--<a class="mega-menu-link" href="#">NOUS CONNAÎTRE</a>-->
                                    <!--</li>-->
                                    <!--<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>-->
                                    <!--<a class="mega-menu-link" href="CEQUEVOUSOFFREMETALREC.html">CE QUE VOUS OFFRE METALREC</a>-->
                                    <!--</li>-->
                                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>
                                        <a class="mega-menu-link" href="{{'contact'}}">CONTACT</a>
                                    </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    </div>
</div>
<div id="sf-mobile-slideout-backdrop"></div>