@extends('backend.layout.master')

@section('title', 'Affordable Housing')

@section('content')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Property Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddProperty') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Property Location</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Property Location</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('property_location') }}"
                                                                   name="property_location" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Property Type</label>
                                                            <select name="property_name" class="form-control">
                                                                <option value="Residential">Residential</option>
                                                                <option value="Commercial">Commercial</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Price</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('property_price') }}"
                                                                   name="property_price" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Bathrooms</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('bathrooms') }}"
                                                                   name="bathrooms" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Bedrooms</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('bedrooms') }}"
                                                                   name="bedrooms" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Plot Size</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('plotsize') }}"
                                                                   name="plotsize" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Status</label>
                                                            <select name="property_status" class="form-control">
                                                                <option value="Available">Available</option>
                                                                <option value="Not Available">Not Available</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Mortgage fee range</label>
                                                            <select name="property_mortgagerange" class="form-control">
                                                                @foreach($listrange as $list)
                                                                    <option value="{{$list->id}}">{{$list->property_mortgagerange}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Developer Name</label>
                                                            <select name="property_developer" class="form-control">
                                                                @foreach($listdev as $listdevs)
                                                                <option value="{{$listdevs->developer_name}}">{{$listdevs->developer_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section"><i class="fas fa-image"></i>Property Cover Picture</h4>
                                                <div class="row">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            {{--<label class="card-title" for="exampleInputFile">File input</label>--}}
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="card-body">
                                                                <fieldset class="form-group">
                                                                    <input type="file" class="form-control-file" id="exampleInputFile" name="property_image" required>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Property In details</label>
                                                            <textarea class="form-control" rows="10" name="property_indetails" id="property_indetails"  required>{{ old('property_indetails') }}</textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Property Gallery Gallery</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="propertygallery[]">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Property Location</th>
                                                    <th>Property Type</th>
                                                    <th>Property Price</th>
                                                    <th>Property Bathrooms</th>
                                                    <th>Property Bedrooms</th>
                                                    <th>Property Plot Size</th>
                                                    <th>Property Status</th>
                                                    <th>Property Mortgage fee range</th>
                                                    <th>Property Cover Picture</th>
                                                    <th>Property In details</th>
                                                    <th>Date Created</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listhouses as $data)
                                                    <tr>
                                                        <td>{{$data->property_location}}</td>
                                                        <td>{{$data->property_name}}</td>
                                                        <td>{{$data->property_price}}</td>
                                                        <td>{{$data->bathrooms}}</td>
                                                        <td>{{$data->bedrooms}}</td>
                                                        <td>{{$data->plotsize}}</td>
                                                        <td>{{$data->property_status}}</td>
                                                        <td>{{$data->property_mortgagerange}}</td>
                                                        <td><img src="PropertyImages/{{$data->property_image}}" style="width:100%"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editnomie{{$data->id}}">View Property General information
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Property General information</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $text = $data->property_indetails;
                                                                            ?>
                                                                            <p><?php echo $text;?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>

                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editplace{{$data->id}}">Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editplace{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditProperty') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-body">
                                                                                    <h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Property Location</h4>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Property Location</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->property_location}}"
                                                                                                       name="property_location" >
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput2">Property Type</label>
                                                                                                <select name="property_name" class="form-control">
                                                                                                    <option value="{{$data->property_name}}">{{$data->property_name}}</option>
                                                                                                    <option value="Residential">Residential</option>
                                                                                                    <option value="Commercial">Commercial</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Price</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->property_price}}"
                                                                                                       name="property_price">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Bathrooms</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->bathrooms}}"
                                                                                                       name="bathrooms">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Bedrooms</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->bedrooms}}"
                                                                                                       name="bedrooms">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Plot Size</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->plotsize}}"
                                                                                                       name="plotsize" required>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Status</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->property_status}}"
                                                                                                       name="property_status">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Mortgage fee range</label>
                                                                                                <select name="property_mortgagerange" class="form-control">
                                                                                                    <option value="{{$data->property_mortgagerange}}">{{$data->property_mortgagerange}}</option>
                                                                                                    <option value="50 000 frw - 200,000 frw">50 000 frw - 100,000 frw</option>
                                                                                                    <option value="100 000 frw - 150,000 frw">100 000 frw - 150,000 frw</option>
                                                                                                    <option value="150 000 frw - 200,000 frw">150 000 frw - 200,000 frw</option>
                                                                                                    <option value="200 000 frw - 250,000 frw">200 000 frw - 250,000 frw</option>
                                                                                                    <option value="250 000 frw - 250,000 frw">250 000 frw - 300,000 frw</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                                                    <h4 class="form-section"><i class="fas fa-image"></i>Property Cover Picture</h4>
                                                                                    <div class="row">
                                                                                        <div class="card">
                                                                                            <div class="card-header">
                                                                                                {{--<label class="card-title" for="exampleInputFile">File input</label>--}}
                                                                                            </div>
                                                                                            <div class="card-block">
                                                                                                <div class="card-body">
                                                                                                    <fieldset class="form-group">
                                                                                                        <input type="file" class="form-control-file" id="exampleInputFile" name="property_image">
                                                                                                    </fieldset>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Property In details</label>
                                                                                                <textarea class="form-control" rows="10" name="property_indetails" id="property_indetails"  required>{{$data->property_indetails}}</textarea>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                                </div>

                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteProperty',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Summernote Click to edit end -->
                    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>--}}

                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                            type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
