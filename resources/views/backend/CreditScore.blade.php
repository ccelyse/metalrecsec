@extends('backend.layout.master')

@section('title', 'Affordable Housing')

@section('content')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Occupation</th>
                                                    <th>Phone Number</th>
                                                    <th>Email</th>
                                                    <th>Employer Name</th>
                                                    <th>Property Price</th>
                                                    <th>Property Status</th>
                                                    <th>Property Mortgage fee range</th>
                                                    <th>Property Cover Picture</th>
                                                    <th>Comment</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listapp as $data)
                                                    <tr>
                                                        <td>{{$data->firstname}}</td>
                                                        <td>{{$data->lastname}}</td>
                                                        <td>{{$data->occupation}}</td>
                                                        <td>{{$data->phone}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->employername}}</td>
                                                        <td>{{$data->property_price}}</td>
                                                        <td>{{$data->property_status}}</td>
                                                        <td>{{$data->property_mortgagerange}}</td>
                                                        <td><img src="PropertyImages/{{$data->property_image}}" style="width:100%"></td>
                                                        <td>

                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editplace{{$data->id}}">Credit Score Comment
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editplace{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Credit Score Comment</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            @if($data->credit_score_comment == null)
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('CreditScoreComment') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-body">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="projectinput1" class="form-control"
                                                                                                       name="id" value="{{$data->id}}" hidden>
                                                                                                <textarea id="projectinput1" class="form-control" name="credit_score_comment" rows="5"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                                </div>
                                                                            </form>
                                                                            @else
                                                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('CreditScoreComment') }}" enctype="multipart/form-data">
                                                                                    {{ csrf_field() }}
                                                                                    <div class="form-body">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                    <input type="text" id="projectinput1" class="form-control"
                                                                                                           name="id" value="{{$data->id}}" hidden>
                                                                                                    <textarea id="projectinput1" class="form-control" name="credit_score_comment" rows="5">{{$data->credit_score_comment}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                                    </div>
                                                                                </form>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Summernote Click to edit end -->
                    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>--}}

                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                            type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
