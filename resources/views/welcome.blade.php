@extends('layouts.master')

@section('title', 'Metalrec')

@section('content')
    @include('layouts.topmenu')
        <div class="swift-slider-outer">
            <div id="swift-slider-1" class="swift-slider swiper-container" data-type="slider" data-fullscreen="0" data-max-height="600" data-transition="slide" data-loop="true" data-slide-count="2" data-autoplay="" data-continue="0">
                <div class="swiper-wrapper">
                    <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/pexels-photo-1474993.jpeg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                        <div class="overlay" style="-ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=80)';filter: alpha(opacity=80);-moz-opacity: 0.8;-khtml-opacity: 0.8;opacity: 0.8;background-color: #222222;"></div>
                        <div class="caption-wrap container">
                            <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                                <div class="caption-excerpt">
                                    <p>
                                        <section class="row ">
                                            <div class="spb-animated-headline spb_content_element col-sm-12">
                                                <div class="spb-asset-content">
                                                    <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">
                                                        Le recyclage de tous les déchets, <i><br />
                                                            industriels contenant des </i>
                                                        <span class="sf-words-wrapper">
														métaux précieux
													</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <br />
                                        <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="#knowmore" target="_self"><span class="text">en savoir plus</span><i class="sf-icon-read-more"></i></a>
                                        <!--<a class="sf-button standard white bordered  sf-button-rounded sf-button-has-icon" href="http://uplift.swiftideas.com/purchase" target="_self"><span class="sf-button-border"></span><span class="text">Purchase now</span><i class="sf-icon-cart"></i></a>-->
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/met.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                        <div class="overlay" style="-ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=80)';filter: alpha(opacity=80);-moz-opacity: 0.8;-khtml-opacity: 0.8;opacity: 0.8;background-color: #222222;"></div>
                        <div class="caption-wrap container">
                            <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                                <div class="caption-excerpt">
                                    <p>
                                        <section class="row ">
                                            <div class="spb-animated-headline spb_content_element col-sm-12">
                                                <div class="spb-asset-content">
                                                    <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">
                                                        Le recyclage de tous les déchets, <i><br />
                                                            industriels contenant des </i>
                                                        <span class="sf-words-wrapper">
														métaux précieux
													</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <br />
                                        <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="#knowmore" target="_self"><span class="text">en savoir plus</span><i class="sf-icon-read-more"></i></a>
                                        <!--<a class="sf-button standard white bordered  sf-button-rounded sf-button-has-icon" href="http://uplift.swiftideas.com/purchase" target="_self"><span class="sf-button-border"></span><span class="text">Purchase now</span><i class="sf-icon-cart"></i></a>-->
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide color-slide dynamic-header-change" data-slide-id="2" data-slide-title="breakthrough" style="background-image: url('images/387c2d_6506bf2e08b6408d86d374815b5887dc.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_intro-team.jpg" data-style="light" data-header-style="light">
                        <div class="overlay" style="-ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)';filter: alpha(opacity=50);-moz-opacity: 0.5;-khtml-opacity: 0.5;opacity: 0.5;background-color: #222222;"></div>
                        <div class="caption-wrap container">
                            <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                                <div class="caption-excerpt">
                                    <p>
                                        <section class="row ">
                                            <div class="spb-animated-headline spb_content_element col-sm-12">
                                                <div class="spb-asset-content">
                                                    <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">
                                                        Le recyclage de tous les déchets, <i><br />
                                                            industriels contenant des </i>
                                                        <span class="sf-words-wrapper">
														métaux précieux
													</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <br />
                                        <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="#knowmore" target="_self"><span class="text">en savoir plus</span><i class="sf-icon-read-more"></i></a>
                                        <!--<a class="sf-button standard white bordered  sf-button-rounded sf-button-has-icon" href="http://uplift.swiftideas.com/purchase" target="_self"><span class="sf-button-border"></span><span class="text">Purchase now</span><i class="sf-icon-cart"></i></a>-->
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="swift-slider-prev" href="index.html#">
                    <svg version="1.1" class="svg-swift-slider-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                        <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
			M14,24L34,4L14,24z" />
                        <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
			M14,24l20,20L14,24z" />
                    </svg>
                    <h4>Previous</h4></a>
                <a class="swift-slider-next" href="index.html#">
                    <svg version="1.1" class="svg-swift-slider-next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                        <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
			M34,24L14,44L34,24z" />
                        <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
			M34,24L14,4L34,24z" />
                    </svg>
                    <h4>Next</h4></a>
                <div class="swift-slider-pagination">
                    <div class="dot"><span class=""></span></div>
                    <div class="dot"><span class=""></span></div>
                    <div class="dot"><span class=""></span></div>
                </div>
                <div id="swift-slider-loader" class="circle">
                    <div class="sf-svg-loader">
                        <object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
                    </div>
                </div>
            </div>
        </div>

        <div id="main-container" class="clearfix">
            <div class="page-heading page-heading-hidden page-heading-breadcrumbs clearfix">
                <div class="container">
                    <div class="heading-text">
                        <h1 class="entry-title">Home: Classic</h1>
                    </div>
                    <div id="breadcrumbs">
                        <div class="container">
                            <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Uplift." href="index.html" class="home"><span property="name">Uplift</span></a>
                            <meta property="position" content="1">
                            </span><i class="fa-angle-right"></i><span property="itemListElement" typeof="ListItem"><span property="name">Home: Classic</span>
                            <meta property="position" content="2">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container-wrap" id="knowmore">
                <div class="inner-page-wrap has-no-sidebar no-bottom-spacing clearfix">
                    <div class="clearfix">
                        <div class="page-content hfeed clearfix">
                            <div class="clearfix post-13177 page type-page status-publish hentry" id="13177">
                                <section data-header-style="" class="row fw-row  dynamic-header-change">
                                    <div class="spb-row-container spb-row-content-width col-sm-12  col-natural" data-row-style="dark" data-v-center="false" data-top-style="none" data-bottom-style="none" style="background-color:#f7f7f7;padding-left:0%;padding-right:0%;margin-top:0px;margin-bottom:0px;">
                                        <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                            <section class="row fw-row ">
                                                <div class="divider-wrap col-sm-12">
                                                    <div class="spb_divider thin spb_content_element " style="margin-top: 0px; margin-bottom: 90px;"></div>
                                                </div>
                                            </section>
                                            <section class="container ">
                                                <div class="row">
                                                    <div class="spb_content_element col-sm-6 spb_text_column mt0 mb0">
                                                        <div class="spb-asset-content" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 4%!important;padding-bottom: 0%!important;">
                                                            <div class="title-wrap">
                                                                <h3 class="spb-heading spb-text-heading"><span>L'entreprise</span></h3></div>
                                                            <p>Metalrec intervient sur pratiquement tous les continents pour valoriser les déchets d’industrie contenant des métaux précieux.</p>
                                                            <p>Depuis plus de 30 ans, son expertise lui a permit de collaborer avec des secteurs d’activités aussi variés que :</p>
                                                            <ul>
                                                                <li>Joaillerie</li>
                                                                <li>Orfevrerie</li>
                                                                <li>Electronique</li>
                                                                <li>Petrochimie</li>
                                                                <li>Chimie et bien plus</li>
                                                            </ul>
                                                            <p>Et ce dans le strict respect de l’environnement et des normes internationnales.</p>
                                                            <h6><span style="color: #7eced5;"><a class="read-more" style="color: #7eced5;" href="index.html#">plus à propos L' entreprise</a></span></h6>

                                                        </div>
                                                    </div>
                                                    <div class="team_list carousel-asset spb_content_element col-sm-6">
                                                        <div class="spb-asset-content">
                                                            <video width="100%" height="400"  controls>
                                                                <source src="images/MetalRec.mp4" type="video/mp4">
                                                                <!--<source src="movie.ogg" type="video/ogg">-->
                                                                <!--Your browser does not support the video tag.-->
                                                            </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="row fw-row ">
                                                <div class="divider-wrap col-sm-12">
                                                    <div class="spb_divider thin spb_content_element " style="margin-top: 60px; margin-bottom: 0px;"></div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </section>
                                <div class="link-pages"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="sf-full-header-search-backdrop"></div>
        </div>
        <div id="footer-wrap">
            <footer id="footer">
                <div class="container">
                    <div id="footer-widgets" class="row clearfix">
                        <div class="col-md-6">
                            <section id="text-3" class="widget widget_text clearfix" style="display: inline-grid;">
                                <div class="textwidget">
                                    <img class="alignleft size-full wp-image-14782" src="images/metalrec.png" alt="uplift_logo_white" />
                                </div>
                                <p style="display: inline-grid;">
                                    <strong>©2018 METALREC</strong>
                                </p>
                            </section>
                        </div>


                        <div class="col-md-6" style="display: flex;justify-content: flex-end;">
                            <section id="text-5" class="widget widget_text clearfix">
                                <div class="widget-heading title-wrap clearfix">
                                    <h3 class="spb-heading"><span>Get in touch</span></h3></div>
                                <div class="textwidget">Avenue Louise 149/24 1050 BRUXELLES BELGIEN
                                    <br> Tél : +32 (0) 2 3107125
                                    <br> Email: <a href="#"><span class="__cf_email__">contact.metalrec@gmail.com </span></a>
                                    <br>
                                    <br>
                                    {{--<ul class="social-icons standard ">--}}
                                        {{--<li class="twitter"><a href="#" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>--}}
                                        {{--<li class="facebook"><a href="#" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>--}}
                                        {{--<!--<li class="dribbble"><a href="#" target="_blank"><i class="fa-dribbble"></i><i class="fa-dribbble"></i></a></li>-->--}}
                                        {{--<li class="linkedin"><a href="#" target="_blank"><i class="fa-linkedin"></i><i class="fa-linkedin"></i></a></li>--}}
                                        {{--<li class="instagram"><a href="#" target="_blank"><i class="fa-instagram"></i><i class="fa-instagram"></i></a></li>--}}
                                    {{--</ul>--}}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

            </footer>
        </div>
        <div id="back-to-top" class="animate-top"><i class="sf-icon-up-chevron"></i></div>

        <div class="fw-video-area">
            <div class="fw-video-close"><i class="sf-icon-remove"></i></div>
            <div class="fw-video-wrap"></div>
        </div>
        <div class="fw-video-spacer"></div>

        <div id="sf-included" class="has-products has-carousel has-parallax has-team stickysidebars "></div>
        <div id="sf-option-params" data-slider-slidespeed="7000" data-slider-animspeed="600" data-slider-autoplay="0" data-slider-loop="" data-carousel-pagespeed="800" data-carousel-slidespeed="200" data-carousel-autoplay="0" data-carousel-pagination="0" data-lightbox-nav="default" data-lightbox-thumbs="1" data-lightbox-skin="light" data-lightbox-sharing="1" data-product-zoom-type="lens" data-product-slider-thumbs-pos="left" data-product-slider-vert-height="659" data-quickview-text="Quickview" data-cart-notification="tada" data-username-placeholder="Username" data-email-placeholder="Email" data-password-placeholder="Password" data-username-or-email-placeholder="Username or email address" data-order-id-placeholder="Order ID" data-billing-email-placeholder="Billing Email"></div>
        <div class="sf-svg-loader">
            <object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
        </div>
        <div id="loveit-locale" data-ajaxurl="http://uplift.swiftideas.com/wp-admin/admin-ajax.php" data-nonce="2e85738f5b" data-alreadyloved="You have already loved this item." data-error="Sorry, there was a problem processing your request." data-loggedin="false"></div>
        <script data-cfasync="false" src="cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script>
        <script type="text/javascript">
            setTimeout(function() {
                var a = document.createElement("script");
                var b = document.getElementsByTagName('script')[0];
                a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0049/1794.js";
                a.async = true;
                a.type = "text/javascript";
                b.parentNode.insertBefore(a, b)
            }, 1);
        </script>
        <div class="sf-container-overlay">
            <div class="sf-loader">
                <div class="sf-svg-loader">
                    <object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var onLoad = {
                init: function() {

                    "use strict";

                    // Variables
                    var trigger = jQuery('#sf-styleswitch-trigger'),
                        triggerWidth = trigger.width(),
                        switcher = jQuery('#sf-style-switcher'),
                        switchCont = switcher.find('.switch-cont'),
                        isAnimating = false;

                    // Loaded
                    trigger.css('width', '60px').fadeIn(400).addClass('loaded');

                    // Hove in/out
                    trigger.on('mouseover', function() {
                        if (switcher.hasClass('open') || switcher.hasClass('animating')) {
                            return;
                        }
                        switcher.css('width', '');
                        trigger.transition({
                            width: triggerWidth
                        }, 400, 'easeOutCirc');
                    });
                    trigger.on('mouseleave', function() {
                        if (switcher.hasClass('open')) {
                            return;
                        }
                        if (!switcher.hasClass('open') && switcher.hasClass('animating')) {
                            setTimeout(function() {
                                trigger.transition({
                                    width: '60'
                                }, 400, 'easeOutCirc');
                            }, 600);
                        } else {
                            trigger.transition({
                                width: '60'
                            }, 400, 'easeOutCirc');
                        }
                    });

                    // Open switcher window
                    trigger.on('click', function(e) {

                        e.preventDefault();

                        if (isAnimating) {
                            return;
                        }

                        isAnimating = true;
                        switcher.addClass('animating');

                        switcher.toggleClass('open');
                        setTimeout(function() {
                            isAnimating = false;
                            switcher.removeClass('animating');
                        }, 600);
                    });
                    // Close switcher window

                    // Switcher controls

                    if (jQuery('#header-section').length > 0) {
                        var currentHeader = jQuery('#header-section').attr('class').split(' ')[0];
                        jQuery(".header-select option[value=" + currentHeader + "]").prop("selected", "selected")
                    }

                    jQuery('.header-select').change(function() {
                        var baseURL = onLoad.getPathFromUrl(location.href),
                            newURLParam = "?header=" + jQuery('.header-select').val();

                        location.href = baseURL + newURLParam;
                    });

                    jQuery('.color-select li').on('click', 'a', function(e) {
                        e.preventDefault();

                        jQuery('.color-select li').removeClass('active');
                        jQuery(this).parent().addClass('active');

                        var selectedColor = '#' + jQuery(this).data('color');
                        var s = "#ff0000";
                        var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
                        var matches = patt.exec(selectedColor);
                        var top = "rgba(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + ",0.60)";
                        var bottom = "rgba(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + ",1.0)";

                        // background-color
                        jQuery('.sf-accent-bg, .funded-bar .bar, .flickr-widget li, .portfolio-grid li, figcaption .product-added, .woocommerce .widget_layered_nav ul li.chosen small.count, .woocommerce .widget_layered_nav_filters ul li a, span.highlighted, #one-page-nav li .hover-caption, #sidebar-progress-menu ul li.reading .progress, .loading-bar-transition .pace .pace-progress, input[type=submit], button[type=submit], input[type="file"], .wpcf7 input.wpcf7-submit[type=submit], .sf-super-search .search-options .ss-dropdown ul, av ul.menu > li.menu-item.sf-menu-item-btn > a, .shopping-bag-item a > span.num-items, .bag-buttons a.checkout-button, .bag-buttons a.create-account-button, .woocommerce input.button.alt, .woocommerce .alt-button, .woocommerce button.button.alt, #jckqv .cart .add_to_cart_button, #fullscreen-supersearch .sf-super-search .search-go a.sf-button, #respond .form-submit input[type=submit], .sf-button.accent:not(.bordered), .sf-icon-box-animated .back, .spb_icon_box_grid .spb_icon_box .divider-line, .tabs-type-dynamic .nav-tabs li.active a, .progress .bar, .mejs-controls .mejs-time-rail .mejs-time-current, .team-member-divider, .masonry-items li.testimonial .testimonial-text, .spb_tweets_slider_widget .tweet-icon i, .woocommerce .cart button.add_to_cart_button.product-added, .woocommerce .single_add_to_cart_button:disabled[disabled], .woocommerce .order-info, .woocommerce .order-info mark, .woocommerce .button.checkout-button, .woocommerce #review_form #respond .form-submit input, .woocommerce button[type="submit"], .woocommerce input.button, .woocommerce a.button, .woocommerce-cart table.cart input.button, .review-order-wrap #payment #place_order, #buddypress .pagination-links span, #buddypress .load-more.loading a, #bbp-user-navigation ul li.current a, .bbp-pagination-links span.current').css('background-color', selectedColor);
                        // color
                        jQuery('.sf-accent, .portfolio-item .portfolio-item-permalink, .read-more-link, .blog-item .read-more, .author-link, span.dropcap2, .spb_divider.go_to_top a, #header-translation p a, span.dropcap4, #sidebar-progress-menu ul li.reading a, .read-more-button, .player-controls button.tab-focus, .player-progress-played[value], .sf-super-search .search-options .ss-dropdown > span, .sf-super-search .search-options input, .author-bio a.author-more-link, .comment-meta-actions a, .blog-aux-options li.selected a, .sticky-post-icon, .blog-item .author a.tweet-link, .side-post-info .post-share .share-link, a.sf-button.bordered.accent, .progress-bar-wrap .progress-value, .sf-share-counts .share-text h2, .sf-share-counts .share-text span, .woocommerce div.product .stock, .woocommerce form .form-row .required, .woocommerce .widget_price_filter .price_slider_amount .button, .product-cat-info a.shop-now-link, .woocommerce-cart table.cart input[name="apply_coupon"], .woocommerce .shipping-calculator-form button[type="submit"], .woocommerce .cart input.button[name="update_cart"]').css('color', selectedColor);
                        // border-color
                        jQuery('.sf-accent-border,span.dropcap4, .super-search-go, .sf-button.accent, .sf-button.accent.bordered .sf-button-border, blockquote.pullquote, .woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select, .woocommerce .woocommerce-info, .woocommerce-page .woocommerce-info, .woocommerce-cart table.cart input[name="apply_coupon"], .woocommerce .shipping-calculator-form button[type="submit"], .woocommerce .cart input.button[name="update_cart"], #buddypress .activity-header a, #buddypress .activity-read-more a, #buddypress .pagination-links span, #buddypress .load-more.loading a, #bbp-user-navigation ul li.current a, .bbp-pagination-links span.current').css('border-color', selectedColor);
                        jQuery('.spb_impact_text .spb_call_text, code > pre').css('border-left-color', selectedColor);
                        jQuery('#account-modal .nav-tabs li.active span, .sf-super-search .search-options .ss-dropdown > span, .sf-super-search .search-options input').css('border-bottom-color', selectedColor);
                        jQuery('#bbpress-forums li.bbp-header').css('border-top-color', selectedColor);
                        // stroke
                        jQuery('.sf-hover-svg path').css('stroke', selectedColor);

                        console.log('-webkit-gradient(linear,left top,left bottom,from(' + top + ') 25%,to(' + bottom + ') 100%)');
                        jQuery('figure.animated-overlay figcaption').css('background', '-webkit-gradient(linear,left top,left bottom,color-stop(25%,' + top + '),to(' + bottom + '))');
                        jQuery('figure.animated-overlay figcaption').css('background', '-webkit-linear-gradient(top,' + top + ' 25%, ' + bottom + ' 100%)');
                        jQuery('figure.animated-overlay figcaption').css('background', 'linear-gradient(to bottom,' + top + ' 25%, ' + bottom + ' 100%)');

                        jQuery('figcaption .thumb-info-alt > i, .gallery-item figcaption .thumb-info > i, .gallery-hover figcaption .thumb-info > i').css('color', selectedColor);

                    });

                },
                getURLVars: function() {
                    var vars = [],
                        hash;
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                    for (var i = 0; i < hashes.length; i++) {
                        hash = hashes[i].split('=');
                        vars.push(hash[0]);
                        vars[hash[0]] = hash[1];
                    }
                    return vars;
                },
                getPathFromUrl: function(url) {
                    return url.split("?")[0];
                }
            };

            jQuery(document).ready(onLoad.init);
        </script>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/core.min-ver=1.11.4.js'></script>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/widget.min-ver=1.11.4.js'></script>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/button.min-ver=1.11.4.js'></script>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/spinner.min-ver=1.11.4.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var jckqv = {
                "ajaxurl": "http:\/\/uplift.swiftideas.com\/wp-admin\/admin-ajax.php",
                "nonce": "6ed2845eef",
                "settings": {
                    "styling_autohide": "0",
                    "styling_hoverel": ".product",
                    "styling_icon": "eye",
                    "styling_text": "Quickview",
                    "styling_btnstyle": "flat",
                    "styling_padding": ["8", "10", "8", "10"],
                    "styling_btncolour": "#66cc99",
                    "styling_btnhovcolour": "#47C285",
                    "styling_btntextcolour": "#ffffff",
                    "styling_btntexthovcolour": "#ffffff",
                    "styling_borderradius": ["4", "4", "4", "4"],
                    "position_autoinsert": "1",
                    "position_position": "afteritem",
                    "position_align": "left",
                    "position_margins": ["0", "0", "10", "0"],
                    "general_method": "click",
                    "imagery_imgtransition": "horizontal",
                    "imagery_transitionspeed": "600",
                    "imagery_autoplay": "0",
                    "imagery_autoplayspeed": "3000",
                    "imagery_infinite": "1",
                    "imagery_navarr": "1",
                    "imagery_thumbnails": "thumbnails",
                    "content_showtitle": "1",
                    "content_showprice": "1",
                    "content_showrating": "1",
                    "content_showbanner": "1",
                    "content_showdesc": "short",
                    "content_showatc": "1",
                    "content_ajaxcart": "1",
                    "content_autohidepopup": "1",
                    "content_showqty": "1",
                    "content_showmeta": "1",
                    "content_themebtn": "0",
                    "content_btncolour": "#66cc99",
                    "content_btnhovcolour": "#47C285",
                    "content_btntextcolour": "#ffffff",
                    "content_btntexthovcolour": "#ffffff",
                    "general_gallery": "1",
                    "general_overlaycolour": "#000000",
                    "general_overlayopacity": "0.8"
                },
                "imgsizes": {
                    "catalog": {
                        "width": "700",
                        "height": "791",
                        "crop": 1
                    },
                    "single": {
                        "width": "700",
                        "height": "791",
                        "crop": 1
                    },
                    "thumbnail": {
                        "width": "120",
                        "height": "136",
                        "crop": 1
                    }
                },
                "url": "http:\/\/uplift.swiftideas.com",
                "text": {
                    "added": "Added!",
                    "adding": "Adding to Cart...",
                    "loading": "Loading..."
                }
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/jck-woo-quickview/assets/frontend/js/main.min-ver=4.9.6.js'></script>
        <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.min-ver=3.51.0-2014.06.20.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var _wpcf7 = {
                "loaderUrl": "http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif",
                "recaptchaEmpty": "Please verify that you are not a robot.",
                "sending": "Sending ...",
                "cached": "1"
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts-ver=4.4.2.js'></script>
        <script type='text/javascript' src='wp-content/plugins/swift-framework/includes/page-builder/frontend-assets/js/spb-functions.js'></script>
        <script type='text/javascript' data-cfasync="true" src='wp-content/plugins/swift-framework/includes/swift-slider/assets/js/swift-slider.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                "ajax_url": "\/wp-admin\/admin-ajax.php",
                "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%",
                "i18n_view_cart": "View Cart",
                "cart_url": "http:\/\/uplift.swiftideas.com\/cart\/",
                "is_cart": "",
                "cart_redirect_after_add": "no"
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min-ver=2.5.5.js'></script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min-ver=2.70.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var woocommerce_params = {
                "ajax_url": "\/wp-admin\/admin-ajax.php",
                "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%"
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min-ver=2.5.5.js'></script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min-ver=1.4.1.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                "ajax_url": "\/wp-admin\/admin-ajax.php",
                "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%",
                "fragment_name": "wc_fragments"
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min-ver=2.5.5.js'></script>
        <script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min-ver=1.2.0.js'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var yith_wcwl_l10n = {
                "ajax_url": "\/wp-admin\/admin-ajax.php",
                "redirect_to_cart": "no",
                "multi_wishlist": "",
                "hide_add_button": "1",
                "is_user_logged_in": "",
                "ajax_loader_url": "http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
                "remove_from_wishlist_after_add_to_cart": "yes",
                "labels": {
                    "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
                    "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
                },
                "actions": {
                    "add_to_wishlist_action": "add_to_wishlist",
                    "remove_from_wishlist_action": "remove_from_wishlist",
                    "move_to_another_wishlist_action": "move_to_another_wishlsit",
                    "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
                }
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl-ver=2.0.15.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/bootstrap.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery-ui-1.10.2.custom.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/owl.carousel.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/theme-scripts.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/ilightbox.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery.isotope.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/imagesloaded.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery.infinitescroll.min.js'></script>
        <script type='text/javascript' src='wp-content/themes/uplift/js/functions.js'></script>
        <script type='text/javascript' src='wp-includes/js/comment-reply.min-ver=4.9.6.js'></script>
        <script type='text/javascript' src='wp-includes/js/hoverIntent.min-ver=1.8.1.js'></script>
        <script type='text/javascript' src='wp-includes/js/wp-embed.min-ver=4.9.6.js'></script>

        </body>

        </html>
@endsection